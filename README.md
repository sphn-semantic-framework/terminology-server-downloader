# terminology-server-downloader

This tool provides a command line interface to download SPHN external terminologies
from https://terminology-server.dcc.sib.swiss/.

## Prerequisites
* Credentials for the terminology server are required and can be obtained at dcc@sib.swiss.
* Python >=3.8
* pip or pip3
* gnupg

## Install
To install from a specific [release tag](https://git.dcc.sib.swiss/sphn-semantic-framework/terminology-server-downloader/-/tags):
```
pip3 install git+https://git.dcc.sib.swiss/sphn-semantic-framework/terminology-server-downloader.git@<release_tag>
```

Developers can install the package from `main` branch:
```
pip3 install git+https://git.dcc.sib.swiss/sphn-semantic-framework/terminology-server-downloader.git
```

## Usage

### Configuration
You need to pass the host and your credentials to the terminology server
via a local `.env` file with a contents of the following shape:

```
MINIO_SERVER="terminology-server.dcc.sib.swiss"
MINIO_PORT="9000"
TS_USER="<terminology-server-username>"
TS_PASSWORD="<terminology-server-password>"
HTTPS_PROXY="<optional-proxy-server>"
```

Description of the parameters in the `.env` file:
* MINIO_SERVER: URL to the MinIO server.
* MINIO_PORT: Port to access the MinIO service.
* TS_USER: Username to access the MinIO service.
* TS_PASSWORD: Password to access the MinIO service.
* HTTPS_PROXY [optional]: Proxy server (see below).

In this code repository, you find a `internal.env.sample` with template values.

### Download data

* Download all packages from the terminology server to `~/data`: `terminology-server-downloader ~/data`
* Download only packages in the  "current/" folder on the terminology server to `~/data`: `terminology-server-downloader ~/data -p current/`

### CLI full help
```
$ terminology-server-downloader -h
Usage: terminology-server-downloader [OPTIONS] TARGET_BASE_DIR

  Downloads external terminologies from a MinIO-based terminology-server
  with all packages (.zip and .gpg files) in the bucket, verifies
  signature and unzips content.

  * All packages will be downloaded to TARGET_BASE_DIR/archive

  * Packages in the "current/" folder on the server will addionally be linked
  to TARGET_BASE_DIR/current

  * For each package on the server, if there exists a package directory in the
  TARGET_BASE_DIR/archive, the download is skipped. Note that only filenames
  are checked, not content.

  * For each file on the server, if a file with the same name exists in the
  --working_dir, the download is skipped. Note that only filenames are
  checked, not content.

  Configuration
  =============
  You need to pass the host and your credentials to the terminology server
  via a local `.env` file with a contents of the following shape:

  ```
  MINIO_SERVER="terminology-server.dcc.sib.swiss"
  MINIO_PORT="9000"
  TS_USER="<terminology-server-username>"
  TS_PASSWORD="<terminology-server-password>"
  HTTPS_PROXY="<optional-proxy-server>"
  ```

  Description of the parameters in the `.env` file:
  * MINIO_SERVER: URL to the MinIO server.
  * MINIO_PORT: Port to access the MinIO service.
  * TS_USER: Username to access the MinIO service.
  * TS_PASSWORD: Password to access the MinIO service.
  * HTTPS_PROXY [optional]: Proxy server (see below).

  Proxy
  =====
  If you need to go via a proxy, set the `https_proxy` env var accordingly, 
  before running the downloader.

  `export https_proxy=https://PROXYSERVER:PROXYPORT/`

  Examples
  ========
  Download all packages from the terminolgy server to ~/data:
  `terminology-server-downloader ~/data`

  Download only packages in the  "current/" folder on the terminology server to ~/data:
  `terminology-server-downloader ~/data -p current/`

Options:
  --version                   Show the version and exit.
  -w, --working_dir PATH      directory to download files from server.  If not
                              provided, files will be downloaded to temp dir
  -p, --package-prefix TEXT   Prefix to filter packges. E.g.,
                              'archive/ontologies-2021-10' will download all
                              packges starting with that string. By default,
                              all packages are downloaded.
  --cleanup / --no-cleanup    Remove zip and gpg files. --no-cleanup is
                              ignored if no  --working_dir is provided (since
                              a temp  dir will be used as  working dir)
                              [default: cleanup]
  --trusted-fingerprint TEXT  Fingerprint of gpg signature  [default:
                              8D33EAABB425ADF27A713B7E8BAC7A01F395B738]
  --bucket-name TEXT          Minio bucket name  [default: ontologies]
  -v, --verbose               Show debug info
  -h, --help                  Show this message and exit.
```

### Running the terminology-server-downloader as Docker container
We provide here an example for writing the terminologies to a folder `data` on the host system's current path:
```
docker run -it --rm -v "./internal.env:/deploy/.env" -v "./data:/terminologies" -v "./gnupg:/gnupg" -e GNUPGHOME=/gnupg --user "$(id -u):$(id -g)" terminology_server_downloader:latest --bucket-name ontologies -p archive/ontologies-2024-04-23_10-10-07 --trusted-fingerprint 8D33EAABB425ADF27A713B7E8BAC7A01F395B738
```

#### Note
There exists only the user root in the container. So, the files are written as the root user. However, you can specify the desired GID for accessing the files.
The default file and folder permission is inherited as 755 from the terminology server.
Remember we cannot use arbitrary UID because there is no real home directory, where gpg can create a `.gnupg` directory, here via wrapping Python package `gpg-lite`.

When another user than root should be used, the folders `data` and `gnupg` must exist before running the container because they would be autogenerated by the Docker deamon (run typically as root).


#### Docker compose
Run more complex docker compose commands via the following configuration **considering the note above** (especially about the required folders `data` and `gnupg`):

```
docker compose --env-file docker.env run --remove-orphans --service-ports terminology_server_downloader -p archive/ontologies-2024-04-23_10-10-07 --trusted-fingerprint 8D33EAABB425ADF27A713B7E8BAC7A01F395B738
```

### Setting up a `cron` job
To automatically download the terminologies on a regular basis, you can set up a
 `cron` job. Replace the following paths in the example below with targets on 
 your filesystem
```
<credentials-file>: /path/to/credentials/.env
<venv-path>: /path/to/venv/
<output-path>: /path/to/output/
```


#### Create a credentials file
Store your credentials in a dedicated file that can be read by the cronjob. 
Set permissions so that other uses cannot see your credentials
```
install -m 700 /dev/null <credentials-file>
cat <<EOF > <credentials-file>
export TS_USER=<terminology-server-username>
export TS_PASSWORD=<terminology-server-password>
EOF
```

#### Add a cronjob
E.g., using `crontab -e`
```
0 3 * * * BASH_ENV=<credentials-file> bash -c "source <venv-path>/bin/activate && terminology-server-downloader <output-path> -p current"
``` 
In this example, all ontologies in the `current/` directory on the terminology 
server will be fetched and stored in the <output-path> each night at 3:00.

## Development
### Run tests
To test downloading from the terminolgy server and the signature checks, the tests
implemented in `test_downloader.py` can be used.
Install `pytest` (`pip install pytest`) to run the tests.
The credentials to connect to the server need to be provided.

```
export TS_USER=<terminology-server-username>
 export TS_PASSWORD=<terminology-server-password>
```

```
pytest -v 
pytest -v -s # to display stdout
pytest -vs -k TestArchive # to run a single class...
```

### Release repo
* Change version in `__init__`
* Commit
* Release: 

```
git tag <release_tag>
git push --tags
```
* Change version in `__init__` to `<release_tag>dev`
* Commit

## Changes
* v0.0.3: fix broken link from current to archive if target path is relative
* v0.0.2: added proxy support
* v0.0.1: initial

## License
The project is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) license.

## Contact
For any question or comment, please create an issue.