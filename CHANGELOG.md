# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unpublished]

### Added
- The CLI comes now with an additional option to select the keyserver.
- The terminology server downloader can be built and run as a Docker container. The written files are however owned by root as default. When running docker with a specific `UID:GID`, the files are owned and accessible by that user and group, respectively:
  ```
  docker run -it --rm -v "./internal.env:/deploy/.env" -v "./data:/terminologies" -v "./gnupg:/gnupg" -e GNUPGHOME=/gnupg --user "$(id -u):$(id -g)" terminology_server_downloader:latest --bucket-name ontologies -p archive/ontologies-2024-04-23_10-10-07 --trusted-fingerprint 8D33EAABB425ADF27A713B7E8BAC7A01F395B738
  ```

### Changed
- The default keyserver is hkp://keys.openpgp.org, given that DCC's keyserver is shut down.

### Fixed
- Solved issue in case no `https_proxy` is specified.


## [0.0.4]

### Changed
- Instead of environment variables, we use now a .env file.
- Added new variables for more modular environment:
  ```
  MINIO_SERVER="terminology-server.dcc.sib.swiss"
  MINIO_PORT="9000"
  TS_USER="<terminology-server-username>"
  TS_PASSWORD="<terminology-server-password>"
  HTTPS_PROXY="<optional-proxy-server>"
  ```
