import logging
from pathlib import Path
from getpass import getpass

import click

from .__init__ import __version__
from . import main
from .config import settings

CONTEXT_SETTINGS = {"help_option_names": ["-h", "--help"]}


@click.command(context_settings=CONTEXT_SETTINGS)
@click.version_option(__version__)
@click.argument("target_base_dir", type=Path)
@click.option(
    "-w",
    "--working_dir",
    type=Path,
    default=None,
    help="""Directory to download packed files from server.
If not provided, files will be downloaded to temp dir""",
    show_default=True,
)
@click.option(
    "-p",
    "--package-prefix",
    type=str,
    default=None,
    help="""Prefix to filter packges. E.g., 'archive/ontologies-2021-10' will download all
packges starting with that string. By default, all packages are downloaded.
""",
    show_default=True,
)
@click.option(
    "--cleanup/--no-cleanup",
    type=bool,
    default=True,
    help="""Remove zip and gpg files. --no-cleanup is ignored if no
--working_dir is provided (since a temp dir will be used as working dir)
""",
    show_default=True,
)
@click.option(
    "--trusted-fingerprint",
    type=str,
    default="CB5931C8B32B4BB7DE6D781C95B54DA2D018F7E7",
    help="Fingerprint of PGP-signature",
    show_default=True,
)
@click.option(
    "--keyserver",
    type=str,
    default="hkp://keys.openpgp.org",
    help="Keyserver for PGP-signature lookup",
    show_default=True,
)
@click.option(
    "--bucket-name",
    type=str,
    default="ontologies",
    help="Minio bucket name",
    show_default=True,
)
@click.option("-v", "--verbose", is_flag=True, help="Show debug info")
def cli(
    target_base_dir: Path,
    trusted_fingerprint: str,
    keyserver: str,
    bucket_name: str,
    working_dir: Path,
    cleanup: bool,
    package_prefix: str,
    verbose: bool,
):
    """
    Downloads external terminologies from a MinIO-based terminology-server
    with all packages (.zip and .gpg files) in the bucket, verifies
    signature and unzips content.

    * All packages will be downloaded to TARGET_BASE_DIR/archive

    * Packages in the "current/" folder on the server will addionally be linked
    to TARGET_BASE_DIR/current

    * For each package on the server, if there exists a package directory in the
    TARGET_BASE_DIR/archive, the download is skipped. Note that only filenames
    are checked, not content.

    * For each file on the server, if a file with the same name exists in the
    --working_dir, the download is skipped. Note that only filenames are
    checked, not content.

    \b
    Configuration
    =============
    You need to pass the host and  your credentials to the terminology server
    via a local `.env` file with a contents of the following shape:

    \b
    ```
    MINIO_SERVER="terminology-server.dcc.sib.swiss"
    MINIO_PORT="9000"
    TS_USER="<terminology-server-username>"
    TS_PASSWORD="<terminology-server-password>"
    HTTPS_PROXY="<optional-proxy-server>"
    ```

    \b
    Description of the parameters in the `.env` file:
    * MINIO_SERVER: URL to the MinIO server.
    * MINIO_PORT: Port to access the MinIO service.
    * TS_USER: Username to access the MinIO service.
    * TS_PASSWORD: Password to access the MinIO service.
    * HTTPS_PROXY [optional]: Proxy server (see below).

    \b
    Proxy
    =====
    If you need to go via a proxy, set the `https_proxy` env var accordingly,
    before running the downloader.

    \b
    `export https_proxy=https://PROXYSERVER:PROXYPORT/`

    \b
    Examples
    ========
    Download all packages from the terminolgy server to ~/data:
    `terminology-server-downloader ~/data`

    \b
    Download only packages in the  "current/" folder on the terminology server to ~/data:
    `terminology-server-downloader ~/data -p current/`

    """

    formatter = {
        "format": "%(asctime)s %(name)-25s %(levelname)-8s %(message)s",
        "datefmt": "%Y-%m-%d %H:%M:%S",
    }
    if verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO
    logging.basicConfig(level=level, **formatter)
    logging.info(f"Running {__name__}, version {__version__}")

    # Allow for fallback options
    if settings.TS_USER is None:
        logging.info(f"Waiting for user input TS_USER")
        settings.TS_USER = input("Username for Terminology Server:")
    if settings.TS_PASSWORD is None:
        logging.info(f"Waiting for user input TS_PASSWORD")
        settings.TS_PASSWORD = getpass("Password for Terminology Server:")

    main.fetch_packages(
        target_base_dir,
        trusted_fingerprint,
        keyserver,
        bucket_name,
        working_dir,
        cleanup,
        package_prefix,
    )


if __name__ == "__main__":
    cli()
