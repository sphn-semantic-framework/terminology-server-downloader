from typing import Union, Optional
import logging
from pathlib import Path
from zipfile import ZipFile


def unzip_file(zip_file: Union[str, Path], target_dir: Optional[Path] = None):
    with ZipFile(zip_file, "r") as f:
        f.extractall(target_dir)
    logging.debug(f"\t{zip_file} unpacked to {target_dir}")


def package_to_out_path(package: Union[str, Path], target_dir: Union[str, Path] = Path(".")):
    """
    package can be
      - a file (e.g, "current/ontologies-2022-02-07_15-21-22.zip"), or
      - a package (e.g, "current/ontologies-2022-02-07_15-21-22"),
    will return filepath with stripped first sub-folder,
    e.g, -> "<target_dir>/ontologies-2022-02-07_15-21-22", or
         -> "ontologies-2022-02-07_15-21-22" (if no target_dir provided)
    """
    filename = Path(package).name
    file_path = Path(target_dir) / filename
    return file_path
