from typing import Optional, List
import logging
import os
from pathlib import Path

import urllib3
from minio import Minio

from .config import settings
from .utils import package_to_out_path


def connect() -> Minio:
    if settings.HTTPS_PROXY:
        proxy = settings.HTTPS_PROXY
        http_client = urllib3.ProxyManager(proxy)
        logging.info(f"Going via proxy {proxy}")
    elif "https_proxy" in os.environ and os.environ["https_proxy"] is not None:
        proxy = os.environ["https_proxy"]
        http_client = urllib3.ProxyManager(proxy)
        logging.info(f"Going via system defined proxy {proxy}")
    else:
        http_client = None

    # https://min.io/docs/minio/linux/developers/python/API.html
    client = Minio(
        settings.MINIO_SERVER + ":" + settings.MINIO_PORT,
        access_key=settings.TS_USER,
        secret_key=settings.TS_PASSWORD,
        http_client=http_client,
    )
    return client


def discover_packages(
    client: Minio, bucket_name: str, package_prefix: Optional[str] = None
) -> List[str]:
    """
    a package consists of
    - <package_name>.gpg, and
    - <package_name>.zip
    """
    objects = list(
        client.list_objects(
            bucket_name=bucket_name, prefix=package_prefix, recursive=True
        )
    )
    files = [o.object_name for o in objects]

    packages = list(set([str(Path(f).with_suffix("")) for f in files]))
    packages.sort()
    n_files = len(files)
    n_packages = len(packages)
    if n_packages == 0:
        raise RuntimeError(f"No packages found with prefix '{package_prefix}'")

    logging.debug(f"{n_packages} packages found.")
    assert n_files == (2 * n_packages), "n_files != 2*n_packages"
    return packages


def download_package(client, bucket_name, package, download_dir: Path) -> None:
    """
    downloads all files of a package
    strips sub-folder
    "current/ontologies-2022-02-07_15-21-22.zip" -> "<download_dir>/ontologies-2022-02-07_15-21-22.zip"
    """
    if not isinstance(package, str):
        raise TypeError(f"`package` is not of type `str`, but {type(package)}")

    objects = list(
        client.list_objects(bucket_name=bucket_name, prefix=package, recursive=True)
    )
    if len(objects) != 2:
        raise RuntimeError(
            f"expected 2 files, but {len(objects)} found: {[o.object_name for o in objects]}"
        )

    for obj in objects:
        object_name = obj.object_name
        file_path = package_to_out_path(object_name, download_dir)

        logging.debug(f"\tDownloading {object_name} to {file_path}")
        if file_path.is_file():
            logging.debug(f"\t{file_path} already exists. Overwritting file!")

        client.fget_object(
            bucket_name=bucket_name, object_name=object_name, file_path=str(file_path)
        )
