"""
This module is responsible for downloading terminology data from the terminology server.

Versioning:
We use semantic versioning for this module.
For more information on semantic versioning, refer to https://semver.org/.
"""

__version__ = "0.0.4"
