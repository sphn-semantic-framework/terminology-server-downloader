from typing import Optional

from pydantic_settings import BaseSettings, SettingsConfigDict


# class Settings(BaseSettings):
#     class Config:
#         case_sensitive = True
#         env_file = ".env"

#     MINIO_SERVER: str = "terminology-server.dcc.sib.swiss"
#     TS_USER: str = ""
#     TS_PASSWORD: str = ""
#     HTTPS_PROXY: str | None = None


class Settings(BaseSettings):
    """Setting from file `.env`.

    Following the documentation:
    https://docs.pydantic.dev/dev/concepts/pydantic_settings/#dotenv-env-support
    """

    model_config = SettingsConfigDict(case_sensitive=True, env_file=".env")

    TS_USER: Optional[str] = None
    TS_PASSWORD: Optional[str] = None
    MINIO_SERVER: str = "terminology-server.dcc.sib.swiss"
    MINIO_PORT: str = "9000"
    HTTPS_PROXY: Optional[str] = None


settings = Settings()
