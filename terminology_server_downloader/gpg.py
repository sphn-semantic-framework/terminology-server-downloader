# gpg cli equivalent of verifying the signature

# Example of good signature

# $ gpg --verify ontologies-2021-10-22_18-53-51.gpg ontologies-2021-10-22_18-53-51.zip
# gpg: Signature made Fr 22 Okt 20:54:08 2021 CEST
# gpg:                using RSA key 8D33EAABB425ADF27A713B7E8BAC7A01F395B738
# gpg: Good signature from "DCC Terminology Server <shubham.kapoor@sib.swiss>" [unknown]
# gpg: WARNING: This key is not certified with a trusted signature!
# gpg:          There is no indication that the signature belongs to the owner.
# Primary key fingerprint: 9AE9 CCC5 964D 88B2 79DB  94AD 337F 6741 B6CC 05DE
#      Subkey fingerprint: 8D33 EAAB B425 ADF2 7A71  3B7E 8BAC 7A01 F395 B738

# Example of bad signature
# $ gpg --verify ontologies-2021-10-22_18-53-51.gpg ontologies-2021-10-29_09-45-54.zip
# gpg: Signature made Fr 22 Okt 20:54:08 2021 CEST
# gpg:                using RSA key 8D33EAABB425ADF27A713B7E8BAC7A01F395B738
# gpg: BAD signature from "DCC Terminology Server <shubham.kapoor@sib.swiss>" [unknown]

import logging

import gpg_lite as gpg

error_template = """***Error from GPG:***
{error}
******
Unable to verify signature
"""


class GPGError(gpg.cmd.GPGError):
    pass


def verify_signature(zip_file, gpg_file, trusted_fingerprint):
    """
    Verifies that zip_file has been signed with gpg_file signature and
    that it has been signed with the trusted_fingerprint.
    If not OK, raises GPGError
    """
    gpg_store = gpg.GPGStore()

    logging.debug(f"\tVerifying signature for {zip_file} with {gpg_file}")
    try:
        with open(zip_file, mode="rb") as zip_f, open(gpg_file, mode="rb") as gpg_f:
            signee_fingerprint = gpg_store.verify_detached_sig(
                src=zip_f, sig=gpg_f.read()
            )
        logging.debug(f"\tSignature valid!")
    except gpg.cmd.GPGError as e:
        raise GPGError(error_template.format(error=e))

    if signee_fingerprint != trusted_fingerprint:
        raise GPGError(
            f"Signee fingerprint is not trusted fingerprint {signee_fingerprint} vs {trusted_fingerprint}"
        )
    else:
        logging.debug(
            f"\tSignee fingerprint is trusted fingerprint {signee_fingerprint}"
        )


def fetch_public_gpg_key(fingerprint: str, keyserver: str):
    """
    Fetches public pgp key if not available locally
    """
    gpg_store = gpg.GPGStore()
    keys = gpg_store.list_pub_keys(search_terms=(fingerprint,))
    if not keys:
        logging.debug(f"Downloading public gpg key {fingerprint} from {keyserver}")
        gpg_store.recv_keys(fingerprint, keyserver=keyserver)
    else:
        logging.debug(f"Public gpg key {fingerprint} found locally")
