from typing import Union, Optional, List, Tuple
import logging
import os
import shutil
from tempfile import TemporaryDirectory
from pathlib import Path

from .gpg import fetch_public_gpg_key, verify_signature
from .minio_utils import Minio, connect, discover_packages, download_package
from .utils import package_to_out_path, unzip_file


def init(target_dir: Path, trusted_fingerprint: str, keyserver: str) -> Minio:
    """
    * initialized connection to minio server
    * creates target dir
    * fetches public gpg key
    """
    target_dir.mkdir(exist_ok=True, parents=True)
    client = connect()

    fetch_public_gpg_key(trusted_fingerprint, keyserver=keyserver)
    return client


def handle_working_dir(
    cleanup: bool, working_dir: Optional[Path]
) -> Tuple[Path, Optional[TemporaryDirectory]]:
    """
    Create working dir if provided. Otherwise get a temp dir.
    """
    if not working_dir:
        tmp_dir_obj = TemporaryDirectory()
        working_dir = Path(tmp_dir_obj.name)
        if not cleanup:
            logging.warn(
                f"You have provided no WORKING_DIR and have set CLEANUP to False. \
The downloaded data will be stored in a temp dir and will not persist this process. \
Provide a WORKING_DIR if the zip/gpg files downloaded from the server should persist."
            )
    else:
        tmp_dir_obj = None
        Path(working_dir).mkdir(exist_ok=True, parents=True)
    return working_dir, tmp_dir_obj


def link_current_package(
    target_base_dir: Path, target_dir: Path, packages: List[str]
) -> None:
    """
    For all packages starting with 'current/', creates a symlink in
    {target_base_dir}/current/
    """
    current_packages = [p for p in packages if p.startswith("current/")]
    if current_packages:
        current_dir = target_base_dir / "current"
        if current_dir.is_dir():
            shutil.rmtree(current_dir)
        current_dir.mkdir(exist_ok=True, parents=True)
        for current_package in current_packages:
            current_package_name = current_package.lstrip("current/")
            package_link_dir = current_dir / current_package_name
            package_target_dir = target_dir / current_package_name
            package_target_dir_rel = os.path.relpath(
                package_target_dir, package_link_dir.parent
            )
            logging.debug(f"Linking {package_link_dir} to {package_target_dir}")
            package_link_dir.symlink_to(package_target_dir_rel)


def fetch_package(
    package: str,
    download_dir: Union[str, Path],
    target_dir: Union[str, Path],
    trusted_fingerprint: str,
    keyserver: str,
    bucket_name: str,
    cleanup: bool = True,
    client: Minio = None,
    info_str: str = "",
) -> bool:
    """
    Download a single package in the bucket to TARGET_BASE_DIR/archive.
    Verifies signature and unzips content.
    """
    target_dir = Path(target_dir)
    if not client:
        client = init(target_dir, trusted_fingerprint, keyserver)

    if info_str:
        info_str += " "

    logging.info(f"{info_str}Working on {package}")

    package_name = package_to_out_path(package)
    if (target_dir / package_name).is_dir():
        logging.info(
            f"\tPackage {package_name} already exists in {target_dir}. \
Skipping download"
        )
        package_downloaded = False
    else:
        download_package(client, bucket_name, package, download_dir)

        file_prefix = package_to_out_path(package, download_dir)
        zip_file = file_prefix.with_suffix(".zip")
        gpg_file = file_prefix.with_suffix(".gpg")

        verify_signature(zip_file, gpg_file, trusted_fingerprint)

        unzip_file(zip_file, target_dir / zip_file.with_suffix("").name)

        if cleanup:
            for file in [zip_file, gpg_file]:
                logging.debug(f"\tRemoving {file}")
                file.unlink()
        logging.info("\tDone.")
        package_downloaded = True
    return package_downloaded


def fetch_packages(
    target_base_dir: Union[str, Path],
    trusted_fingerprint: str,
    keyserver: str,
    bucket_name: str,
    working_dir: Optional[Path] = None,
    cleanup: bool = True,
    package_prefix: str = None,
) -> None:
    """
    Download and process all packages from the terminology server.
    Packages can be filtered via {package_prefix}
    """
    target_base_dir = Path(target_base_dir)
    target_dir = target_base_dir / "archive"

    working_dir, tmp_dir_obj = handle_working_dir(cleanup, working_dir)
    logging.info(
        f"Data will be downloaded to {working_dir} and unpacked to \
{target_dir}"
    )

    client = init(target_dir, trusted_fingerprint, keyserver)

    # discover
    packages = discover_packages(client, bucket_name, package_prefix)

    logging.debug(packages)
    downloaded_packages: list[str] = []
    for i, package in enumerate(packages):
        info_str = f"{i+1}/{len(packages)}"
        package_downloaded = fetch_package(
            package,
            working_dir,
            target_dir,
            trusted_fingerprint,
            keyserver,
            bucket_name,
            cleanup,
            client,
            info_str,
        )
        if package_downloaded:
            downloaded_packages.append(package)
    link_current_package(target_base_dir, target_dir, downloaded_packages)

    if tmp_dir_obj:
        logging.debug(f"Removing temp. working dir {working_dir}")
        tmp_dir_obj.cleanup()
