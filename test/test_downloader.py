import os
import shutil
from pathlib import Path

import pytest

from terminology_server_downloader.gpg import GPGError, verify_signature
from terminology_server_downloader.main import fetch_packages


class TestCurrent:
    """
    Test download and processing of the "current/" minio package.
    """

    @pytest.fixture
    def test_download_current(self, tmp_path):
        """
        This tests if download and processing of the "current/" package works. Since we cannot no the name of the package
        in advance, we just check if there is an archive directory in the target_dir.
        """
        target_dir = tmp_path / "target"
        working_dir = tmp_path / "wd"

        fetch_packages(
            target_dir,
            working_dir=working_dir,
            trusted_fingerprint="8D33EAABB425ADF27A713B7E8BAC7A01F395B738",
            bucket_name="ontologies",
            package_prefix="current/",
        )

        tree_found = [p.relative_to(target_dir) for p in list(target_dir.rglob("*"))]
        assert Path("archive") in tree_found

        yield target_dir, working_dir

        # teardown
        shutil.rmtree(target_dir)
        shutil.rmtree(working_dir)

    @pytest.mark.skip
    def test_current_link(self, test_download_current):
        """
        We check that the ontology in "current/" is a link to the one in "archive/"
        """
        target_dir, working_dir = test_download_current

        in_archive = list(target_dir.glob("archive/*"))
        assert len(in_archive) == 1, "expected only one ontology"

        in_current = list(target_dir.glob("current/*"))
        assert len(in_current) == 1, "expected only one ontology"

        assert in_current[0].resolve() == in_archive[0].resolve()


class TestCurrentRelPath:
    """
    Test download and processing of the "current/" minio package.
    """

    @pytest.fixture
    def test_download_current_relpath(self, tmp_path):
        """
        This tests if download and processing of the "current/" package works. Since we cannot no the name of the package
        in advance, we just check if there is an archive directory in the target_dir.
        """
        os.chdir(tmp_path)

        target_dir = Path("target")
        working_dir = Path("wd")

        fetch_packages(
            target_dir,
            working_dir=working_dir,
            trusted_fingerprint="8D33EAABB425ADF27A713B7E8BAC7A01F395B738",
            bucket_name="ontologies",
            package_prefix="current/",
        )

        tree_found = [p.relative_to(target_dir) for p in list(target_dir.rglob("*"))]
        assert Path("archive") in tree_found

        yield target_dir.absolute(), working_dir.absolute()

        # teardown
        shutil.rmtree(target_dir)
        shutil.rmtree(working_dir)

    @pytest.mark.skip
    def test_current_link_relpath(self, test_download_current_relpath):
        """
        We check that the ontology in "current/" is a link to the one in "archive/"
        """
        target_dir, working_dir = test_download_current_relpath

        in_archive = list(target_dir.glob("archive/*"))
        assert len(in_archive) == 1, "expected only one ontology"

        in_current = list(target_dir.glob("current/*"))
        assert len(in_current) == 1, "expected only one ontology"

        assert in_current[0].resolve() == in_archive[0].resolve()


@pytest.fixture
def download_data(tmp_path):
    """
    Download two ontologies known to be in the 'archive/' folder on minio.
    """
    target_dir = tmp_path / "target"
    working_dir = tmp_path / "wd"

    fetch_packages(
        target_dir,
        working_dir=working_dir,
        cleanup=False,
        trusted_fingerprint="8D33EAABB425ADF27A713B7E8BAC7A01F395B738",
        bucket_name="ontologies",
        package_prefix="archive/ontologies-2022-04-19_17-55-20",
    )

    fetch_packages(
        target_dir,
        working_dir=working_dir,
        cleanup=False,
        trusted_fingerprint="8D33EAABB425ADF27A713B7E8BAC7A01F395B738",
        bucket_name="ontologies",
        package_prefix="archive/ontologies-2022-04-19_17-32-31",
    )
    yield target_dir, working_dir

    # teardown
    shutil.rmtree(target_dir)
    shutil.rmtree(working_dir)


class TestArchive:
    @pytest.mark.skip
    def test_archive(self, download_data):
        """
        Checks the trees of the two ontologies.
        """
        target_dir, working_dir = download_data

        expected_tree = [
            Path("archive"),
            Path("archive/ontologies-2022-04-19_17-32-31"),
            Path("archive/ontologies-2022-04-19_17-55-20"),
            Path("archive/ontologies-2022-04-19_17-32-31/ICD-10-GM"),
            Path("archive/ontologies-2022-04-19_17-32-31/LOINC"),
            Path("archive/ontologies-2022-04-19_17-32-31/ATC"),
            Path("archive/ontologies-2022-04-19_17-32-31/SNOMED"),
            Path("archive/ontologies-2022-04-19_17-32-31/UCUM"),
            Path("archive/ontologies-2022-04-19_17-32-31/CHOP"),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/ICD-10-GM/sphn_icd-10-gm_2021-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/ICD-10-GM/sphn_icd-10-gm_2022-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/LOINC/sphn_loinc_2.722022-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/LOINC/sphn_loinc_2.712021-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/LOINC/sphn_loinc_2.72-1.ttl"
            ),
            Path("archive/ontologies-2022-04-19_17-32-31/ATC/sphn_atc_2016-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-32-31/ATC/sphn_atc_2017-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-32-31/ATC/sphn_atc_2019-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-32-31/ATC/sphn_atc_2020-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-32-31/ATC/sphn_atc_2022-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-32-31/ATC/sphn_atc_2018-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-32-31/ATC/sphn_atc_2021-3.ttl"),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/SNOMED/snomed-ct-20220131.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/SNOMED/snomed-ct-20200731.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/UCUM/sphn_ucum_2021-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/CHOP/sphn_chop_2020-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/CHOP/sphn_chop_2022-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/CHOP/sphn_chop_2019-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/CHOP/sphn_chop_2021-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/CHOP/sphn_chop_2018-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-32-31/CHOP/sphn_chop_2017-1.ttl"
            ),
            Path("archive/ontologies-2022-04-19_17-55-20/ICD-10-GM"),
            Path("archive/ontologies-2022-04-19_17-55-20/LOINC"),
            Path("archive/ontologies-2022-04-19_17-55-20/ATC"),
            Path("archive/ontologies-2022-04-19_17-55-20/SNOMED"),
            Path("archive/ontologies-2022-04-19_17-55-20/UCUM"),
            Path("archive/ontologies-2022-04-19_17-55-20/CHOP"),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/ICD-10-GM/sphn_icd-10-gm_2021-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/ICD-10-GM/sphn_icd-10-gm_2022-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/LOINC/sphn_loinc_2.72-1.ttl"
            ),
            Path("archive/ontologies-2022-04-19_17-55-20/ATC/sphn_atc_2016-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-55-20/ATC/sphn_atc_2017-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-55-20/ATC/sphn_atc_2019-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-55-20/ATC/sphn_atc_2020-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-55-20/ATC/sphn_atc_2022-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-55-20/ATC/sphn_atc_2018-1.ttl"),
            Path("archive/ontologies-2022-04-19_17-55-20/ATC/sphn_atc_2021-3.ttl"),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/SNOMED/snomed-ct-20220131.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/SNOMED/snomed-ct-20200731.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/UCUM/sphn_ucum_2021-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/CHOP/sphn_chop_2020-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/CHOP/sphn_chop_2022-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/CHOP/sphn_chop_2019-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/CHOP/sphn_chop_2021-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/CHOP/sphn_chop_2018-1.ttl"
            ),
            Path(
                "archive/ontologies-2022-04-19_17-55-20/CHOP/sphn_chop_2017-1.ttl"
            ),
        ]

        tree_found = [p.relative_to(target_dir) for p in list(target_dir.rglob("*"))]
        assert set(tree_found) == set(expected_tree)


class TestGPG:
    @pytest.mark.skip
    def test_gpg_good_signature(self, download_data):
        """
        Tests verification of good signature.
        """
        target_dir, working_dir = download_data
        trusted_fingerprint = "8D33EAABB425ADF27A713B7E8BAC7A01F395B738"
        zip_file = working_dir / "ontologies-2022-04-19_17-32-31.zip"
        gpg_file = working_dir / "ontologies-2022-04-19_17-32-31.gpg"
        verify_signature(zip_file, gpg_file, trusted_fingerprint)

    @pytest.mark.skip
    def test_gpg_bad_signature(self, download_data):
        """
        Tests that bad signature raises Error.
        """
        target_dir, working_dir = download_data
        trusted_fingerprint = "8D33EAABB425ADF27A713B7E8BAC7A01F395B738"
        zip_file = working_dir / "ontologies-2022-04-19_17-55-20.zip"
        gpg_file = working_dir / "ontologies-2022-04-19_17-32-31.gpg"
        with pytest.raises(GPGError):
            verify_signature(zip_file, gpg_file, trusted_fingerprint)

    @pytest.mark.skip
    def test_gpg_bad_fingerprint(self, download_data):
        """
        Tests that bad fingerprint raises Error.
        """
        target_dir, working_dir = download_data
        trusted_fingerprint = "xxx"
        zip_file = working_dir / "ontologies-2022-04-19_17-32-31.zip"
        gpg_file = working_dir / "ontologies-2022-04-19_17-32-31.gpg"
        with pytest.raises(GPGError):
            verify_signature(zip_file, gpg_file, trusted_fingerprint)
