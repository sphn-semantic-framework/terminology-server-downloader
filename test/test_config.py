import unittest

from terminology_server_downloader.config import settings


class TestSettings(unittest.TestCase):
    def test_env_settings(self):
        self.assertEqual(
            settings.MINIO_SERVER,
            "localhost",
            msg=f"Unexpected MinIO server definition: {settings.MINIO_SERVER}"
        )

        self.assertEqual(
            settings.TS_USER,
            "Template",
            msg=f"Unexpected user definition: {settings.TS_USER}"
        )

        self.assertEqual(
            settings.TS_PASSWORD,
            "<Password>",
            msg=f"Unexpected password definition: {settings.TS_PASSWORD}"
        )
