FROM python:3.8-slim AS build
LABEL org.opencontainers.image.authors="Peter.Strassmann@id.ethz.ch"

# Build stage
RUN python -m pip install build

WORKDIR /package
COPY terminology_server_downloader terminology_server_downloader
COPY pyproject.toml pyproject.toml
RUN python -m build .


# Deployment stage
FROM python:3.8-slim

WORKDIR /deploy
COPY --from=build /package/dist/terminology_server_downloader-*.whl /deploy/dist/

RUN apt-get update
RUN apt-get install -y gnupg
RUN python -m pip install /deploy/dist/terminology_server_downloader-*.whl

COPY .env.sample /deploy/.env
VOLUME [ "/terminologies" ]

ENTRYPOINT [ \
    "terminology-server-downloader", \
    "--bucket-name", "ontologies", \
    "--package-prefix", "current", \
    "/terminologies" \
]
